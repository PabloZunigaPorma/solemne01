/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.modelo;


/**
 *
 * @author Pzunip
 */
public class Calculadora {

    private int Interes;
    private int Capital;
    private int Cantidad;
    private int calcular;

    /**
     * @return the Capital
     */
    public int getCapital() {
        return Capital;
    }

    /**
     * @param Capital the Capital to set
     */
    public void setCapital(int Capital) {
        this.Capital = Capital;
    }

    /**
     * @return the Interes
     */
    public int getInteres() {
        return Interes;
    }

    /**
     * @param Interes the Interes to set
     */
    public void setInteres(int Interes) {
        this.Interes = Interes;
    }

    /**
     * @return the Cantidad
     */
    public int getCantidad() {
        return Cantidad;
    }

    /**
     * @param Cantidad the Cantidad to set
     */
    public void setCantidad(int Cantidad) {
        this.Cantidad = Cantidad;
    }

    /**
     * @return the calcular
     */
    public int getcalcular() {
        return calcular;
    }

    /**
     * @param calcular the calcular to set
     */
    public void setcalcular(int calcular) {
        this.calcular = calcular;
    }

    public void calcula() {
        this.setcalcular((this.getCapital() * this.getInteres() * this.getCantidad()) / 100);

    }

}
