<%-- 
    Document   : resultado
    Created on : 09-05-2021, 21:01:23
    Author     : Pzunip
--%>

<%@page import="cl.modelo.Calculadora"%>
<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<%
    Calculadora cal = (Calculadora) request.getAttribute("Calculadora");
%>

<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <fieldset> 
            <h1>El Calculo de Interés es: <%=cal.getcalcular()%> </h1>
            <p>
                <button onclick="location.href = 'index.jsp'">Volver</button>
            </p>
        </fieldset> 

    </body>
</html>
